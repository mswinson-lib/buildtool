lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'buildtool/version'

Gem::Specification.new do |spec|
  spec.name          = 'buildtool'
  spec.version       = Buildtool::VERSION
  spec.authors       = ['Mark Swinson']
  spec.email         = ['mark@mswinson.com']
  spec.summary       = 'common build utils for ruby projects'
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'rake', '10.1.1'
  spec.add_runtime_dependency 'rspec', '~> 3.1'
  spec.add_runtime_dependency 'rubocop', '~> 0.28'
  spec.add_runtime_dependency 'rubocop-rspec', '~> 1.2'
  spec.add_runtime_dependency 'simplecov', '~> 0.9.0'
  spec.add_runtime_dependency 'simplecov-json', '~> 0.2'
  spec.add_runtime_dependency 'yard', '~> 0.8'
  spec.add_runtime_dependency 'yardstick', '~> 0.9'
end
