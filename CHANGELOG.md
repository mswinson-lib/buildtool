**03/01/2019**

**v0.2.1**

    fix task build exiting incorrectly when using remote workspace


**02/01/2019**

**v0.2.0**

    env WORKSPACE_DIR defines directory to run tasks in  
    task 'doc' supports loading options from .yardstick.yml  


**05/07/2018**

**v0.1.1**

    fix 'documentation reports output path should be under reports directory'  
    fix 'style reports should be under reports directory'  


**29/06/2018**

**v0.1.0**

    first release


