# rubocop:disable Style/StringHashKeys
require 'yaml'

module Buildtool
  # build configuration
  class Config
    # load configuration
    def self.load
      if File.exist?(configfile)
        source = File.read(configfile)
        data = YAML.safe_load(source)
        new(data)
      else
        new
      end
    end

    # default options
    # @api public
    # @return [Hash]
    def self.default_options
      {
        'outputdir' => 'target',
        'projectname' => File.basename(FileUtils.pwd),
        'artifacts' => []
      }
    end

    # default options - instance alias
    # @api public
    # @return [Hash]
    def default_options
      self.class.default_options
    end

    # workspace directory
    # @api public
    # @return [String]
    def self.workspacedir
      return @workspacedir if @workspacedir

      @workspacedir = (ENV['WORKSPACE_DIR'] || '.').chomp('/')
      @workspacedir
    end

    # workspace directory - instance helper
    # @api public
    # @return [String]
    def workspacedir
      self.class.workspacedir
    end

    # config file
    # @api public
    # @return [String]
    def self.configfile
      "#{workspacedir}/.buildinfo.yml"
    end

    # outputdir
    # @api public
    # @return  [String]
    def outputdir
      @options['outputdir']
    end

    # project name
    # @api public
    # @return [String]
    def projectname
      @options['projectname']
    end

    # specfile path
    # @api public
    # @return [String]
    def specfile
      @options['specfile'] || "#{projectname}.gemspec"
    end

    # artifacts
    # @api public
    # @return [Array]
    def artifacts
      @options['artifacts']
    end

    private

    # initialize config
    # @api private
    # @param options [Hash] options attributes
    def initialize options = nil
      options ||= {}
      @options = default_options.merge(options)
    end
  end
end
