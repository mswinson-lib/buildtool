require 'fileutils'
require 'buildtool/config'

desc 'clean'
task :clean do
  config = Buildtool::Config.load

  workspacedir = config.workspacedir

  outputdir = config.outputdir
  artifacts = config.artifacts
  artifacts << outputdir

  Dir.chdir(workspacedir) do
    artifacts.each do |artifact|
      FileUtils.rm_rf(artifact)
    end
  end
end
