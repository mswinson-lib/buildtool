Dir.glob("#{File.expand_path(__dir__)}/*.rb").each do |f|
  next if File.basename(f) == __FILE__

  require f
end
