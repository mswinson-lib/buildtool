# rubocop:disable Style/HashSyntax
# rubocop:disable Metrics/BlockLength
# rubocop:disable Style/DocumentationMethod
require 'rubygems'
require 'rspec/core/rake_task'
require 'json'
require 'buildtool/config'

namespace :test do
  config = Buildtool::Config.load

  workspacedir = config.workspacedir
  specfile = "#{workspacedir}/#{config.specfile}"
  outputdir = "#{workspacedir}/#{config.outputdir}"

  exit unless File.exist?(specfile)

  spec = Gem::Specification.load(specfile)
  version = spec.version
  name = spec.name

  artifactdir = "#{outputdir}/artifacts/#{name}"
  gemdir = "#{artifactdir}/#{version}"
  reportdir = "#{gemdir}/reports"

  COVERAGESRC = "#{workspacedir}/coverage".freeze
  COVERAGEDEST = "#{reportdir}/coverage".freeze
  RSPECDEST = "#{reportdir}/rspec/".freeze

  def move_reports(src, dest)
    return unless Dir.exist?(src)

    FileUtils.rm_rf(dest) if Dir.exist?(dest)
    FileUtils.mkdir_p(dest)
    FileUtils.mv(Dir.glob("#{src}/*"), dest)
    FileUtils.rm_rf(src)
  end

  RSpec::Core::RakeTask.new(:rspec_integration) do |t|
    t.pattern = "#{workspacedir}/spec/integration/**/*_spec.rb"
    t.verbose = false
    t.rspec_opts = [
      '--format', 'html',
      '--out', "#{RSPECDEST}/rspec.html",
      '--format', 'json',
      '--out', "#{RSPECDEST}/rspec.json",
      '--format', 'documentation',
      '--color'
    ].join(' ')
  end

  RSpec::Core::RakeTask.new(:rspec_unit) do |t|
    t.pattern = "#{workspacedir}/spec/unit/**/*_spec.rb"
    t.verbose = false
    t.rspec_opts = [
      '--format', 'html',
      '--out', "#{RSPECDEST}/rspec.html",
      '--format', 'json',
      '--out', "#{RSPECDEST}/rspec.json",
      '--format', 'documentation',
      '--color'
    ].join(' ')
  end

  desc 'run unit tests'
  task :unit do
    begin
      Dir.chdir(workspacedir) do
        Rake::Task['test:rspec_unit'].invoke
      end
    ensure
      move_reports(COVERAGESRC, "#{COVERAGEDEST}/unit")
    end
  end

  desc 'run integration tests'
  task :integration do
    begin
      Dir.chdir(workspacedir) do
        Rake::Task['test:rspec_integration'].invoke
      end
    ensure
      move_reports(COVERAGESRC, "#{COVERAGEDEST}/integration")
    end
  end

  desc 'run all tests'
  task :all => %i[unit integration]

  Rake::Task['test:rspec_integration'].clear_comments
  Rake::Task['test:rspec_unit'].clear_comments
end
