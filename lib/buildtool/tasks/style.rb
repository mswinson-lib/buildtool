# rubocop:disable Metrics/BlockLength
require 'rubygems'
require 'rubocop/rake_task'
require 'buildtool/config'

namespace :style do
  config = Buildtool::Config.load

  workspacedir = config.workspacedir
  specfile = "#{workspacedir}/#{config.specfile}"
  outputdir = "#{workspacedir}/#{config.outputdir}"

  exit unless File.exist?(specfile)

  spec = Gem::Specification.load(specfile)
  version = spec.version
  name = spec.name

  artifactdir = "#{outputdir}/artifacts/#{name}"
  gemdir = "#{artifactdir}/#{version}"
  reportdir = "#{gemdir}/reports"

  RuboCop::RakeTask.new(:check) do |t|
    t.options = [
      '--format', 'progress',
      '--format', 'offenses',
      '--format', 'json',
      '--out', "#{reportdir}/style/style.json",
      '--format', 'emacs',
      '--out', "#{reportdir}/style/errors-ruby.err",
      '--format', 'html',
      '--out', "#{reportdir}/style/index.html"
    ]

    t.fail_on_error = true
  end

  task :changedir do
    Dir.chdir(workspacedir)
  end

  Rake::Task[:check].enhance([:changedir])
  Rake::Task[:"check:auto_correct"].enhance([:changedir])
end
