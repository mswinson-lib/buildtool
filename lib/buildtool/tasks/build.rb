require 'rubygems'
require 'fileutils'
require 'buildtool/config'

desc 'build gem'
task :build do
  config = Buildtool::Config.load

  workspacedir = config.workspacedir
  specfile = config.specfile
  outputdir = config.outputdir

  Dir.chdir(workspacedir) do
    break unless File.exist?(specfile)

    spec = Gem::Specification.load(specfile)
    version = spec.version
    name = spec.name

    artifactdir = "#{outputdir}/artifacts/#{name}"
    gemdir = "#{artifactdir}/#{version}"

    FileUtils.mkdir_p(gemdir)
    system("gem build #{specfile}")
    FileUtils.mv("#{name}-#{version}.gem", gemdir)
  end
end
