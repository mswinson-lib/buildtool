# rubocop:disable Metrics/BlockLength
require 'rubygems'
require 'yard'
require 'yardstick/rake/measurement'
require 'yardstick/rake/verify'
require 'buildtool/config'

namespace :doc do
  config = Buildtool::Config.load

  workspacedir = config.workspacedir
  specfile = "#{workspacedir}/#{config.specfile}"
  outputdir = "#{workspacedir}/#{config.outputdir}"
  yardstick_config = "#{workspacedir}/.yardstick.yml"

  exit unless File.exist?(specfile)

  spec = Gem::Specification.load(specfile)
  version = spec.version
  name = spec.name

  artifactdir = "#{outputdir}/artifacts/#{name}"
  gemdir = "#{artifactdir}/#{version}"
  apidir = "#{gemdir}/api"
  reportdir = "#{gemdir}/reports"

  options = {}
  options = YAML.load_file(yardstick_config) if File.exist?(yardstick_config)

  YARD::Rake::YardocTask.new do |t|
    t.before = proc {
      Dir.chdir(workspacedir)
    }
    t.name = :build
    t.files = ["#{workspacedir}/lib/**/*.rb"]
    t.options = ['-o', apidir]
  end

  Yardstick::Rake::Measurement.new(:measure, options) do |measurement|
    measurement.output = "#{reportdir}/doc/errors-doc.err"
  end

  Yardstick::Rake::Verify.new(:verify, options)
end
